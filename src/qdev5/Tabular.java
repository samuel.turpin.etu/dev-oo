package qdev5;

public class Tabular
{
    private Number tab[];

    public Tabular(int size)
    {
        tab = new Number[size];
    }

    public void set(int i, Number nbr)
    {
        tab[i] = nbr;
    }

    public Number max()
    {
        Number i = tab[0];
        for(Number nbr : tab)
        {
            if(nbr.doubleValue()>i.doubleValue()) { i=nbr; }
        }
        return i;
    }

    @Override
    public String toString()
    {
        String str = "[";
        boolean isFinal;
        for(Number i : tab)
        {
            try{
                isFinal = i.equals(max());
            } catch (NullPointerException e) {
                System.err.println("null number");
            }
            finally
            {
                isFinal = false;
            }
            str=str+i+(isFinal ? "" : ",");
        }
        str=str+"]";
        return str;
    }
}
