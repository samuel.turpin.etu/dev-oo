package qdev5;

public class NumberFactory
{
    public static Number number(int i)
    {
        return i;
    }

    public static Number number(long l)
    {
        return l;
    }

    public static Number number(float f)
    {
        return f;
    }

    public static Number number(double d)
    {
        return d;
    }

    public static Number number(int i1, int i2)
    {
        return i1/i2;
    }
}