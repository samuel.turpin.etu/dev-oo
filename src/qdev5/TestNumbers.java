package qdev5;

public class TestNumbers
{
    public static void main(String[] args)
    {
        int i1 = 10;
        int i2 = 5;
        Fraction frac = new Fraction(i1, i2);
        System.out.println(frac);
        System.out.println(frac.doubleValue());
        System.out.println(frac.floatValue());
        System.out.println(frac.intValue());
        System.out.println(frac.longValue());
        Tabular tabular = new Tabular(5);
        tabular.set(1, NumberFactory.number(32));
        tabular.set(2, NumberFactory.number(100,3));
        tabular.set(4, NumberFactory.number(3.5));
        System.out.println(tabular);
    }
}
