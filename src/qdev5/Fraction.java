package qdev5;

public class Fraction extends Number
{
    private int num;
    private int denom;

    public Fraction(int n, int d)
    {
        this.num = n;
        this.denom = d;
    }


    @Override
    public int intValue()
    {
        return this.num / this.denom;    
    }

    @Override
    public long longValue()
    {
        return this.num / this.denom;
    }

    @Override
    public float floatValue()
    {
        return this.num / this.denom;
    }

    @Override
    public double doubleValue()
    {
        return this.num / this.denom;
    }   

    @Override
    public String toString()
    {
        return this.num+" divisé par "+this.denom;
    }
}