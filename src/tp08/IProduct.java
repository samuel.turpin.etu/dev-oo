package tp08;

public interface IProduct
{
    double getPrice();
    boolean isPerishable();
}